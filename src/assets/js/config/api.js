import axios from "axios";
import QS from 'qs'

// let baseURl="http://127.0.0.1:3000"
let baseURl='http://www.xiaoqiproject.xyz/api'

function post(url, params) {
    return new Promise(
        function (resolve, reject) {
            axios({
                    url: baseURl + url,
                    method: 'post',
                    data: QS.stringify(params)
                }
            ).then(result => {
                if(result.data.code==200){
                    resolve(result.data)
                }
                else{
                    reject(result.data)
                }
            })
        }
    )
}

function get(url,params){
    return new Promise(
        function (resolve, reject){
            axios({
                url:baseURl+url,
                method:"get",
                params:params,
            }).then(result=>{
                if(result.data.code==200){
                    resolve(result.data)
                }else{
                    reject(result.data)
                }
            })
        }
    )
}


export {
    post,get
}
