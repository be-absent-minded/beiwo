import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'


//配置axios
import axios from "axios";
Vue.prototype.axios=axios;

axios.defaults.baseURL = 'http://www.xiaoqiproject.xyz/api';
// axios.defaults.baseURL = 'http://127.0.0.1:3000';  //开发版本

//配置vant
import Vant from 'vant';
import 'vant/lib/index.css';
Vue.use(Vant);

//消息通知
import { Notify } from 'vant';
Vue.use(Notify);

import { List } from 'vant';
Vue.use(List);




//配置echarts
import*as echarts from "echarts";
Vue.prototype.$echarts = echarts



Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
