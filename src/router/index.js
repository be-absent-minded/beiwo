import Vue from 'vue'
import VueRouter from 'vue-router'
import Index from '../views/Index'
import User from "../views/User";
import Login from "../views/Login"
import Reg from "../views/reg"
import Shop from "../views/Shop";
import Infinite from "../views/Infinite";
import Decoration_inf from "../views/Decoration_inf";
import Compute from "../views/Compute";
import Chart from "../views/Chart";
import details from "../views/details";
import PriceList from "../views/PriceList";
import Comment from "../views/Comment";
import Comment_data from "../views/Comment_data";

Vue.use(VueRouter)

const routes = [
  {
    path: '/comment_data/:id',
    name: 'Comment_data',
    component: Comment_data,
  },
  {
    path: '/comment/:id',
    name: 'Comment',
    component: Comment,
  },
  {
    path: '/pricelist/:id',
    name: 'PriceList',
    component: PriceList,
  },
  {
    path: '/details/:id',
    name: 'details',
    component: details,
  },
  {
    path: '/chart',
    name: 'Chart',
    component: Chart,
  },
  {
    path: '/compute',
    name: 'Compute',
    component: Compute,
  },
  {
    path: '/decoration_inf/:id',
    name: 'Decoration_inf',
    component: Decoration_inf,
  },
  {
    path: '/infinite',
    name: 'Infinite',
    component: Infinite,
  },
  {
    path: '/shop/:i',
    name: 'Shop',
    component: Shop
  },
  {
    path: '/reg',
    name: 'Reg',
    component: Reg
  },
  {
    path: '/login',
    name: 'Login',
    component: Login
  },
  {
    path: '/user',
    name: 'User',
    component: User
  },
  {
    path:'/',
    name:'Index',
    component:Index,
  }
]

const router = new VueRouter({

  routes
})

export default router
